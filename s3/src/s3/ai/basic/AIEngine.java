package s3.ai.basic;

import java.io.IOException;
import java.util.List;

import s3.ai.AI;
import s3.ai.basic.modules.Strategy;
import s3.base.S3;
import s3.base.S3Action;
import s3.entities.WPlayer;

/**
 * This <code>AIEngine</code> is the main implementor of the interface for the
 * AI. It creates the strategy object and calls the game cycle.
 * 
 * @author Walter Gress and Josh Datko
 * 
 */
public class AIEngine implements AI {

	/**
	 * Instance of a strategy, created new each game start.
	 */
	protected Strategy strategy;
	protected String playerID;

	public AIEngine(String playerID){
		this.playerID = playerID;
		
	}
	public void gameStarts() {

		this.strategy = new Strategy();

	}

	@Override
	public void game_cycle(S3 game, WPlayer player, List<S3Action> actions)
			throws ClassNotFoundException, IOException {

		if (actions == null) {
			System.err.println("Actions is null");
		}
		if (strategy == null) {
			System.err.println("Strategy is null");
		}
		if (game == null) {
			System.err.println("Game is null");
		}
		if (player == null) {
			System.err.println("Player is null");
		}
		
		actions.addAll(strategy.cycle(game, player));

	}

	@Override
	public void gameEnd() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getPlayerId() {
		return this.playerID;
	}

}
