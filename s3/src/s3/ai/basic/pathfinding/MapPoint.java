package s3.ai.basic.pathfinding;

import s3.util.Pair;

/**
 * Extends Pair to define unique equals, hashCode and toString. A bit more
 * helpful for specific x,y coordinates
 * 
 * @author Josh Datko
 * 
 */
public class MapPoint extends Pair<Double, Double> {

	public MapPoint(Double a, Double b) {
		super(a, b);

	}

	public boolean equals(Object rhs) {
		if (this.hashCode() == rhs.hashCode())
			return true;
		return false;
	}

	public String toString() {
		return (m_a.toString() + m_b.toString());
	}

	public static final int SEED = 23;

	//Following hashCode code adapted from Effective Java
	public static int hash(int aSeed, double aDouble) {
		return hash(aSeed, Double.doubleToLongBits(aDouble));
	}

	public static int hash(int aSeed, long aLong) {

		return firstTerm(aSeed) + (int) (aLong ^ (aLong >>> 32));
	}

	private static final int fODD_PRIME_NUMBER = 37;

	private static int firstTerm(int aSeed) {
		return fODD_PRIME_NUMBER * aSeed;
	}

	public int hashCode() {
		int result = MapPoint.SEED;
		result = MapPoint.hash(result, this.m_a);
		result = MapPoint.hash(result, this.m_b);

		return result;
	}
}
