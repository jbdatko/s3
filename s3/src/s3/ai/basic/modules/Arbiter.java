package s3.ai.basic.modules;

import java.util.LinkedList;
import java.util.List;

import s3.ai.basic.Request;
import s3.base.S3Action;
import s3.entities.WFootman;

/**
 * The <code>Arbiter</code> decides which request to accept.
 * 
 */
public class Arbiter extends AIModule {

	protected Perception perception;

	public Arbiter(Perception p) {
		this.perception = p;
	}

	/**
	 * Prioritizes the list of requests
	 * 
	 * @param requests
	 *            list of requests from other AI modules
	 * @return actions Returns the list of valid actions that the game
	 *         controller can execute.
	 */
	public List<S3Action> prioritize(List<Request> requests) {

		List<S3Action> actions = new LinkedList<S3Action>();

		// also if any does not use any resources, execute it: (like attacks)
		List<Request> toExecute = new LinkedList<Request>();

		for (Request r : requests)
			if (r.costGold == 0 && r.costWood == 0)
				toExecute.add(r);
		requests.removeAll(toExecute);

		// sort requests, and if the one with highest priority can be satisfied,
		// do it:
		{
			Request highest = null;
			for (Request r : requests) {
				if (highest == null || r.priority > highest.priority) {
					highest = r;
				}
			}

			if (highest != null) {
				if (highest.costGold <= this.perception.getAvailableGold()
						&& highest.costWood <= this.perception
								.getAvailableWood())
					toExecute.add(highest);
			}
		}

		// Filter the toExecute list for actions to the same peasant:
		List<Request> toDelete = new LinkedList<Request>();
		for (Request r1 : toExecute) {
			for (Request r2 : toExecute) {
				if (r1 != r2) {
					if (r1.unitID == r2.unitID) {
						if (r1.priority >= r2.priority
								&& !toDelete.contains(r2)) {
							toDelete.add(r2);
						}
					}
				}
			}
		}

		toExecute.removeAll(toDelete);

		for(Request r:toExecute) {
			actions.add(r.action);
		}
		
		return actions;

	}

}
