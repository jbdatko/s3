package s3.ai.basic.modules;

import java.util.HashMap;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import s3.ai.basic.Request;
import s3.entities.*;

public class Attack extends AIModule {

	protected Logistics logistics;
	protected Perception perception;
	protected int retry_attack_at_cycle = 0;

	Attack(Logistics l, Perception p) {
		this.logistics = l;
		this.perception = p;
	}

	public List<Request> setGoal(
			HashMap<Class<? extends WTroop>, Integer> targetComposition,
			int waveSize, int numberOfBarracks) {
		List<Request> requests = new LinkedList<Request>();

		int attackPriority = 100;

		// Get all the enemy units
		List<WUnit> allEnemyUnits = this.perception.getEnemyUnits();

		HashMap<Class<? extends WTroop>, List<WTroop>> idleTroops = this.perception
				.getIdleTroops();

		List<WTroop> completeIdleList = new LinkedList<WTroop>();

		for (Class<? extends WTroop> troopType : Logistics.troops) {

			completeIdleList.addAll(idleTroops.get(troopType));
		}
		try {
			//put troop strategy in place
			
			if (waveSize <= 0) {
				waveSize = 5;
			}
			
			if (targetComposition.containsKey(WArcher.class)) {
				requests.addAll(this.logistics.request(WArcher.class, targetComposition.get(WArcher.class)));
			} 
			
			if (targetComposition.containsKey(WCatapult.class)) {
				requests.addAll(this.logistics.request(WCatapult.class, targetComposition.get(WCatapult.class)));
			}
			
			if (targetComposition.containsKey(WFootman.class)) {
				requests.addAll(this.logistics.request(WFootman.class, targetComposition.get(WFootman.class)));
			}
			
			if (targetComposition.containsKey(WKnight.class)) {
				requests.addAll(this.logistics.request(WKnight.class, targetComposition.get(WKnight.class)));
			}
			
			//requests.addAll(this.logistics.request(WArcher.class, 100));
			
			
		} catch (NullPointerException e) {
			if (e.toString().contains("satisfied")) {
				// request can't be satisfied, not a big deal right now.
			}
		}

		if (perception.getWavesCompleted() >= 1
				&& this.perception.getNumberOfType(WBarracks.class) < numberOfBarracks) {
			
			try {
			requests.addAll(this.logistics.request(WBarracks.class,
					attackPriority));
			}
			catch (Exception e){
				//Can't be satisfied now, keep going
			}
		}

		if (completeIdleList.size() >= waveSize) {
			// ATTACK!

			this.perception.increaseWaveCompleted();

			int currentCycle = this.perception.game.getCycle();

			if (currentCycle >= this.retry_attack_at_cycle) {

				List<WTroop> canAttack = new LinkedList<WTroop>();
				
				for (WTroop troop : completeIdleList) {
					WUnit enemy = this.perception.findClosestUnitTo(
							allEnemyUnits, troop);

					if (enemy != null) {
						requests.add(this.getAttackRequest(attackPriority,
								troop, enemy));
						
						canAttack.add(troop);
					}

				}

				if (canAttack.size() != completeIdleList.size()) {
					this.retry_attack_at_cycle = this.perception.game
							.getCycle() + 200;
				}

			}

		}

		return requests;

	}

}
