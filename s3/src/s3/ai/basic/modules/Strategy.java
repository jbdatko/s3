package s3.ai.basic.modules;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import s3.ai.basic.Request;
import s3.base.S3;
import s3.base.S3Action;
import s3.entities.*;

public class Strategy extends AIModule{

	protected Economy economy;
	protected Logistics logistics;
	protected Attack attack;
	protected Arbiter arbiter;
	protected Perception perception;
	protected String strategyInUse;

	protected List<Request> requests;
	
	public Strategy(){
		//Default strategy is basic_state_machine

		//example strategies
		//this.setup("basic_state_machine_phase2");
		//this.setup("basic_state_machine");

		this.setup("match_enemy_strategy");
	
	}

	public Strategy(String strategyType) {
		
		//strategyType = "match_enemy_strategy";

		this.setup(strategyType);

	}
	
	//initializes the Strategy Module (called by constructor)
	//pass strategy type. strategy type is a method called by reflection
	//in the execute phase
	protected void setup(String strategyType){
		
		strategyType = "execute_" + strategyType;
		
		this.strategyInUse = strategyType;
		
		
		// Build the modules
		this.perception = new Perception();
		this.arbiter = new Arbiter(this.perception);

		this.logistics = new Logistics(this.arbiter, this.perception);

		this.economy = new Economy(this.logistics,
				this.perception);
		this.attack = new Attack(this.logistics, this.perception);

		requests = new LinkedList<Request>();
	}

	
	//called during game loop
	//calls current strategy using reflection
	public List<S3Action> cycle(S3 game, WPlayer player) {
		// First we update the perception module
		this.perception.update(game, player);
		
		this.requests = new LinkedList<Request>();
		
		//Then we call the desired strategy, using reflection
		for(Method strategy : this.getClass().getDeclaredMethods()){
		

			//System.err.println(strategy.getName());
			
			//This can allow for dynmaic strategy changes, althogh we did not explore this option
			if ( strategy.getName().equals(this.strategyInUse) ){
				try {
					return (List<S3Action>)strategy.invoke(this, null);
				} catch (Exception e){
					System.out.println("Couldn't find the strategy to execute \n");
				}
			}
		}
		
		return null;



	}
	
	
	
	//basic strategy for use 
	protected  List<S3Action> execute_basic_state_machine(){
		
		// Create the list of recommend actions

		
		int numWorkersWood = 2;
		int numWorkersGold = 3;
		int waveSize = 3;
		int numberOfBarracks = 2;

		HashMap<Class<? extends WTroop>, Integer> armyComposition = new HashMap<Class<? extends WTroop>, Integer>();

		armyComposition.put(WArcher.class, new Integer(1));
		
		if (this.perception.getWavesCompleted() == 2){
			//this.strategyInUse = "execute_basic_state_machine_phase2";
		}

		//Execute the strategy
		return this.execute(numWorkersWood, numWorkersGold, armyComposition, waveSize, numberOfBarracks);
		
	}
	
	//another strategy for example
	protected  List<S3Action> execute_basic_state_machine_phase2(){
		//This is just an example of how to switch states or strategies
		// Create the list of recommend actions
		
		
		int numWorkersWood = 2;
		int numWorkersGold = 2;
		int waveSize = 4;
		int numberOfBarracks = 2;

		HashMap<Class<? extends WTroop>, Integer> armyComposition = new HashMap<Class<? extends WTroop>, Integer>();

		//armyComposition.put(WArcher.class, new Integer(2));
		armyComposition.put(WKnight.class, new Integer(1));
		
		
		if (this.perception.getWavesCompleted() == 3){
			//this.strategyInUse = "execute_state_machine_phase2";
			//Maybe do the next thing
			//System.out.println("Time to switch the strategy!?");
			//this.strategyInUse = "execute_match_enemy_strategy";
		
		}

		//Execute the strategy
		return this.execute(numWorkersWood, numWorkersGold, armyComposition, waveSize, numberOfBarracks);
		
	}

	
	
	protected List<S3Action> execute_match_enemy_strategy() {
		
		int numWorkersWood = 2;
		int numWorkersGold = 2;
		int waveSize = 10;
		int numberOfBarracks = 2;
	
		//System.out.println("Executing execute_match_enemy_strategy strategy");
		
		//our units (troops)
		
		HashMap<Class<? extends WTroop>, Integer> armyComposition = new HashMap<Class<? extends WTroop>, Integer>();
		
		
		//Get all enemy troops
		
		// Get all the enemy units
		List<WUnit> allEnemyUnits = this.perception.getEnemyUnits();

		//match the enemies units
		
		for (WUnit u : allEnemyUnits) {
			
			//check to see if unit is a troop
			if (u instanceof WTroop) {
				
				int priority = 0;
				
				//System.err.println("Unit is a troop");
				
				//adds to prioritized troops to army composition
				//based off of the enemy's current units
				
				if (u instanceof WKnight) {		//contains the knight unit
					armyComposition.put(WKnight.class, new Integer(3));
					
				} 
				
				if (u instanceof WCatapult) {
					armyComposition.put(WCatapult.class, new Integer(3));
				}
				

				if (u instanceof WArcher) {
					armyComposition.put(WArcher.class, new Integer(3));
				}
				
				if (u instanceof WFootman) {
					armyComposition.put(WFootman.class, new Integer(3));
				}
				
				else {
					armyComposition.put(WFootman.class, new Integer(3));
				}
				
				
							
			}
			
		}
		
		
		if (this.perception.getWavesCompleted() == 3){
			//this.strategyInUse = "execute_state_machine_phase2";
			//Maybe do the next thing
			//System.out.println("Time to switch the strategy!?");
		}


		//Execute the strategy
		return this.execute(numWorkersWood, numWorkersGold, armyComposition, waveSize, numberOfBarracks);
	}
	
	/**
	 * @param numWorkersWood Target number of workers gathering wood
	 * @param numWorkersGold Target number of workers gather God
	 * @param armyComposition Target army composition
	 * @param waveSize Target number for the wave size
	 * @return The scrubbed list of actions for the game to execute
	 */
	protected List<S3Action> execute(int numWorkersWood, int numWorkersGold,
			HashMap<Class<? extends WTroop>, Integer> armyComposition,
			int waveSize, int numberOfBarracks) {
		
		requests.addAll(economy.setGoal(numWorkersWood, numWorkersGold));
		requests.addAll(attack.setGoal(armyComposition, waveSize, numberOfBarracks));

		return arbiter.prioritize(requests);

	}

}
