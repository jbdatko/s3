package s3.ai.basic.modules;

import s3.ai.basic.Request;
import s3.base.S3Action;
import s3.entities.WBarracks;
import s3.entities.WBuilding;
import s3.entities.WGoldMine;
import s3.entities.WOTree;
import s3.entities.WPeasant;
import s3.entities.WTownhall;
import s3.entities.WTroop;
import s3.entities.WUnit;
import s3.util.Pair;

public abstract class AIModule {

	protected Request getBuildRequest(int priority, WPeasant builder,
			Class<? extends WBuilding> building, Pair<Integer, Integer> loc) {

		WBuilding b = null;
		try {
			b = building.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new Request(priority, builder.entityID, b.getCost_gold(),
				b.getCost_wood(), new S3Action(builder.entityID,
						S3Action.ACTION_BUILD, building.getSimpleName(),
						loc.m_a, loc.m_b));

	}

	protected Request getTroopTrainRequest(int priority, WBarracks barracks,
			Class<? extends WTroop> troopType) {

		S3Action a = new S3Action(barracks.entityID, S3Action.ACTION_TRAIN,
				troopType.getSimpleName());

		WTroop t = null;
		try {
			t = troopType.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new Request(priority, barracks.entityID, t.getCost_gold(),
				t.getCost_wood(), a);

	}

	protected Request getPeasantTrainRequest(int priority, WTownhall townhall) {

		WPeasant p = null;
		try {
			p = WPeasant.class.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new Request(priority, townhall.entityID, p.getCost_gold(),
				p.getCost_wood(), new S3Action(townhall.entityID,
						S3Action.ACTION_TRAIN, WPeasant.class.getSimpleName()));
	}

	protected Request getMineRequest(int priority, WPeasant peasant,
			WGoldMine mine) {

		return new Request(priority, peasant.entityID, 0, 0, new S3Action(
				peasant.entityID, S3Action.ACTION_HARVEST, mine.entityID));
	}

	protected Request getChopRequest(int priority, WPeasant peasant, WOTree tree) {

		return new Request(priority, peasant.entityID, 0, 0, new S3Action(
				peasant.entityID, S3Action.ACTION_HARVEST, tree.getX(),
				tree.getY()));
	}

	protected Request getAttackRequest(int priority, WTroop troop, WUnit enemy) {

		return new Request(priority, troop.entityID, 0, 0, new S3Action(
				troop.entityID, S3Action.ACTION_ATTACK, enemy.entityID));
	}

}
