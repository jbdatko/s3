package s3.ai.basic;

import s3.base.S3Action;

/**
 * <code>Request</code> class copied from Professor for use in the class project
 * 
 */
public class Request {

	public int priority;
	public int unitID;
	public int costGold, costWood;
	public S3Action action;

	public Request(int p, int ID, int g, int w, S3Action a) {
		priority = p;
		unitID = ID;
		costGold = g;
		costWood = w;
		action = a;
	}

}
